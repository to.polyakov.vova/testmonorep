import React from 'react';

import {BrowserRouter} from "react-router-dom";
import { Provider } from 'react-redux'
import store from './store'

import {AppContainer} from "./AppContainer";

 const App = () => {


    return (
        <BrowserRouter>
        <Provider store={store}>
            <AppContainer/>
        </Provider>
        </BrowserRouter>
    );
}

export default App;
