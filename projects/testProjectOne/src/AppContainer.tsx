import React, {FC} from 'react'
import {Routes, Route} from "react-router-dom";

import {Manager} from "@testmonorep/popupManager"
import {BarPopup} from "@componets/barpopup";
import {FooPopup} from '@componets/foopopup'

import {usePresenter} from "./usePresenter";


const config = {
    foo: FooPopup,
    bar: BarPopup
}

const style = {
    backgroundColor: "green",
    zIndex: 1,
    widths: '100Vh',
    height: '100Vh'
}

const btnStyle = {
    zIndex: 2
}


export const AppContainer: FC = () => {
    const {callFoo, close} = usePresenter()

    return <>
        <div style={style}>
            <button data-test="clickMeBtn" style={btnStyle} onClick={callFoo}>Click me =)</button>
        </div>
        <Routes>
            <Route path='/foo' element={<FooPopup close={close}/>}/>
        </Routes>
        <Manager config={config}/>
    </>
}