import { IAppState } from 'store/app/interfaces'
import { TPlayer } from 'store/player/interfaces'


import { CombinedState } from 'redux'

export interface IRootState {
  app: CombinedState<IAppState>
  player: TPlayer
}
