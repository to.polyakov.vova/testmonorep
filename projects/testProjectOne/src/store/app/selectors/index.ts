import { IRootState } from 'store/interfaces'
import { ILanguageData, IResolution, ILangState } from '../interfaces'

const getSettings = (state: IRootState) => state.app

export const getLangInfo = (state: IRootState): ILangState => getSettings(state).langInfo
export const getLanguage = (state: IRootState): ILanguageData => getLangInfo(state).languageData

export const getAppSettings = (state: IRootState): IResolution => getSettings(state).resolution

export const getIsMobile = (state: IRootState): boolean => getAppSettings(state).isMobile
export const getIsTablet = (state: IRootState): boolean => getAppSettings(state).isTablet
export const getIsDesktop = (state: IRootState): boolean => getAppSettings(state).isDesktop
export const getCurrentWidth = (state: IRootState): number => getAppSettings(state).currentWidth
