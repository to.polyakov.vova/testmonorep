import { ILangState, ILanguageData } from '../interfaces'
import { createSlice, PayloadAction } from '@reduxjs/toolkit'

const initialState: ILangState = {
  languageData: {
    value: 'ru',
    label: 'ru',
  },
}
export const langSlice = createSlice({
  name: 'language',
  initialState: initialState,
  reducers: {
    setLanguage: (state, action: PayloadAction<ILanguageData>) => {
      state.languageData = action.payload
    },
  },
})
