import { combineReducers } from '@reduxjs/toolkit'

import { resolutionSlice } from './resolutionReducer'
import { langSlice } from './langReducer'
import { IAppState } from 'store/app/interfaces'

export const reducer = combineReducers<IAppState>({
  langInfo: langSlice.reducer,
  resolution: resolutionSlice.reducer,
})
export const actions = { ...langSlice.actions, ...resolutionSlice.actions }
