import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IResolution } from 'store/app/interfaces'

const appInfoItalState: IResolution = {
  isMobile: false,
  isTablet: false,
  isDesktop: false,
  currentWidth: 0,
}

export const resolutionSlice = createSlice({
  name: 'resolution',
  initialState: appInfoItalState,
  reducers: {
    setResolution: (state, action: PayloadAction<IResolution>) => {
      return { ...state, ...action.payload }
    },
  },
})
