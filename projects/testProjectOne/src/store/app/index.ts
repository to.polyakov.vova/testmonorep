import { actions, reducer } from './reducer'
import * as selectors from './selectors'

export const appActions = actions
export const appReducer = reducer
export const appSelectors = selectors
