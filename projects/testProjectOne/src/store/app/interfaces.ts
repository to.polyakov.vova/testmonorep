export interface ILanguageData {
  value: string
  label: string
}

export interface IResolution {
  isMobile: boolean
  isTablet: boolean
  isDesktop: boolean
  currentWidth: number
}

export interface ILangState {
  languageData: ILanguageData
}

export interface IAppState {
  langInfo:  ILangState,
  resolution: IResolution
}
