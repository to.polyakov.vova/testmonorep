// тут очень и крайне приблизительно

export interface IPlayer {
  address: string
  auth_fields_missed : string[]
  auto_issuing_bonuses : boolean
  avatar : { image_path: boolean }
  can_issue_bonuses : boolean
  cis : boolean
  city : string | null
  confirmed_at :  string | null
  country : string
  country_code : string
  created_at : string
  currency : string
  current_sign_in_at : string
  date_of_birth : string | null
  deposit_bonus_code : string | null
  doc_issue_date : string | null
  doc_issued_by : string | null
  email : string | null
  email_is_confirmed : boolean
  first_name : string | null
  gender : string | null
  id : number
  language : string | null
  last_name : string | null
  middle_name : string | null
  mobile_phone : string | null
  nationality : string | null
  nickname : string | null
  personal_id_number : number | null
  place_of_birth : string | null
  postal_code : string | null
  receive_new_login_email : boolean
  receive_newsletters : boolean
  receive_promos : boolean
  receive_sms_promos : boolean
  region_code : string
  sign_up_source : string
  statuses : string[]
  tags : string[]
  time_zone : string
  two_factor_enabled : boolean
  unsubscribed : boolean
  uuid : string
}

export type TPlayer = Partial<IPlayer>