import { actions, reducer } from './reducer'
import * as selectors from './selectors'

export const playerActions = actions
export const playerReducer = reducer
export const playerSelectors = selectors
