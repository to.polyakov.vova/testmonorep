import { IRootState } from 'store/interfaces'
import { TPlayer } from 'store/player/interfaces'

export const getPlayer = (state: IRootState): TPlayer => state.player

export const getIsAuth = (state: IRootState): boolean => Boolean(state.player.id)