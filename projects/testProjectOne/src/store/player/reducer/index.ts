import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { TPlayer } from '../interfaces'

const initialState: TPlayer = {}

const slice = createSlice({
  name: 'player',
  initialState,
  reducers: {
    setPlayer: (state, action: PayloadAction<TPlayer>) => {
      return {
        ...action.payload
      }
    }
  }
})

export const reducer = slice.reducer
export const actions = slice.actions
