import { createStore } from '@testmonorep/store'
// import { createStore } from './createStore'
import { appReducer as app } from './app'

import { playerReducer as player } from './player'
import { IRootState } from './interfaces'
import { ReducersMapObject } from 'redux'

const reducers: ReducersMapObject<IRootState> = {
  app,
  player,
}

export default createStore<IRootState>({ reducers })
