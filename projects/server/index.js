const express = require('express');
const path = require('path');

const server = express();
server.use(express.static( './build'));

server.get('*', function (request, response) {
    response.sendFile(path.resolve(__dirname, 'build/index.html'));
});



server.listen(3333);