import {EQuery, useQuery} from "@testmonorep/popupManager";

export const useCallPopup = (popupName:string) => {
    const { queries, setParams } = useQuery(EQuery.POPUP)
    const popups = queries ? queries.split(',') : []

    const callPopup = () => {
        popups.push(popupName)
        setParams({[EQuery.POPUP]:popups.join(',')})
    }

    return { callPopup }
}