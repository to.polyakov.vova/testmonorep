import React, {FC} from 'react'

import {PopupLayout} from "@componets/popups-layout"

import {ModalCard} from "@ui-kit/ui-kit"
import {usePresenter} from "./usePresenter"

interface IProps {
    close: () => void
}

export const BarPopup: FC<IProps> = (props) => {
    const {onClick} = usePresenter()
    return <PopupLayout close={props.close} dataTest="barPopup">
        <ModalCard close={props.close} header="BAR Popups">
            <>
                <button onClick={onClick}>add Foo popup</button>
            </>
        </ModalCard>
    </PopupLayout>

}