import {useCallPopup} from "@componets/hooks";

export const usePresenter = () => {
    const {callPopup} = useCallPopup('foo')

    return {onClick: callPopup}
}