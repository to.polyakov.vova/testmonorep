import {useCallPopup} from "@componets/hooks";

export const usePresenter = () => {
    const {callPopup} = useCallPopup('bar')

    return {onClick: callPopup}
}