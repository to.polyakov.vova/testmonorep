import React, {FC} from 'react'

import {AddKeyPressHandler} from '@testmonorep/keyboardhandler'
import {OverlayingPopupLayout, ScrollHider, Portal} from '@ui-kit/ui-kit'

interface IProps {
    close: () => void
    children: React.ReactElement
    dataTest?: string
}

export const PopupLayout: FC<IProps> = ({close, children, dataTest}) => {
    return (
        <>
            <Portal>
                <ScrollHider/>
                <AddKeyPressHandler onEsc={close}/>
                <OverlayingPopupLayout
                    onClose={close}
                    dataTest={dataTest}
                >
                    {children}
                </OverlayingPopupLayout>
            </Portal>
        </>
    )
}
