FROM node:16

WORKDIR /usr/app


COPY . .

COPY package.json .
RUN yarn

CMD ["yarn", "run", "publish:lerna" ]