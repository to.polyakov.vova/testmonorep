# Example of how monorepo helps to split JS applications into multiple packages.  

As a code samples I implemented some of my ideas.
- an approach on how to organise work with popups using react-router and portals.
- why you shouldn't hesitate to separate your code into a different files and folders
- why it is better to use unit-test for testing react hooks and other JS code, and everything connected to the JSX should be tested with e2e or integrations tests  
- how to involve non-coding teammates to write integration or e2e tests using cucumber.js + any test runner you want


If you want to play with it. I would recommend you to clone it and change the git origin to your own repository
For publishing you need to generate your own personal keys and get a project id when you upload code to your git origin
For gitlab its - personal setting - preferences - access tokens
Project id you can find at the main page of the repository.


<code>@testmonorep:registry=https://gitlab.com/api/v4/packages/npm/
@testmonorep:registry=https://gitlab.com/api/v4/projects/{PROJECT_ID}/packages/npm/
'//gitlab.com/api/v4/packages/npm/:_authToken'={ACCESS_TOKEN}
'//gitlab.com/api/v4/projects/{PROJECT_ID}/packages/npm/:_authToken'={ACCESS_TOKEN}</code>



